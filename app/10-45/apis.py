from logging import getLogger

from flask import Blueprint, jsonify

from piste.app.main import app

api_name = '10-45'
url_prefix = '/{}'.format(api_name)

# [Project Gnar]: This variable must be `blueprint` for compatibility with `gnar-gear`
blueprint = Blueprint(api_name, __name__, url_prefix=url_prefix)

log = getLogger(__name__)


# [Project Gnar]: This enpoint is only included in Piste to demonstrate intra-microservice communication
@blueprint.route('', methods=['GET'])
def user_10_45():
    response = app.peer('off-piste').get('check-in').json()
    return jsonify(response)
